import { beforeAll, expect, test } from '@jest/globals';
import Notes from '../Notes';

let notes = new Notes()


describe('Test Notes methods', () => {
  test('Should match notes and letters to 7',  () => {
    const relation = notes.getNotes();
    expect(relation).toHaveLength(7)
  })

  test('Should get La4 frequency', () => {
    const la4Freq = notes.getNoteFreq("La4")
    expect(la4Freq).toEqual(440);
  })
})