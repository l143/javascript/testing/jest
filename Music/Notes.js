const La = 440;
const notes = ['Do', 'Re', 'Mi', 'Fa', 'Sol', 'La', 'Si'];
const letters = ['C', 'D', 'E', 'F', 'G', 'A', 'B'];

export default class Notes {
  getNotes() {
    return notes.map((note, i ) => ({ note, letter: letters[i] }))
  }

  getNoteFreq(noteOvtave) {
    const [note, octave] = noteOvtave
    console.log(note, octave)
  }
}
